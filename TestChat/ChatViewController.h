//
//  ViewController.h
//  TestChat
//
//  Created by Sergey Kim on 29.02.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSQMessagesViewController/JSQMessages.h>

@interface ChatViewController : JSQMessagesViewController

@property (nonatomic, strong) NSString * nickName;

@end

