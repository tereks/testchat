//
//  AuthViewController.m
//  TestChat
//
//  Created by Sergey Kim on 29.02.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "AuthViewController.h"
#import "ChatViewController.h"

@interface AuthViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nicknameField;


@end

@implementation AuthViewController

- (void) viewDidLoad {
    [_nicknameField becomeFirstResponder];
}

- (IBAction)nextButtonSelected:(id)sender {
    if ( _nicknameField.text.length == 0 ) {
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];

        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Ошибка"
                                              message:@"Введите ник"
                                              preferredStyle:UIAlertControllerStyleAlert];

        [alertController addAction:action];

        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }

    [self performSegueWithIdentifier:@"showChat" sender:self];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [segue.identifier isEqualToString:@"showChat"] ) {
        ChatViewController * chatController = (ChatViewController*)segue.destinationViewController;
        chatController.nickName = _nicknameField.text;
    }
}

@end
